Docker
=========

This role will install docker and docker-compose on your server.

platforms
------------

Ubuntu:     
  versions:     
    - focal (20.04)     
    - jammy (22.04)
